package com.codementor;

import javax.crypto.SecretKey;

import io.jsonwebtoken.impl.crypto.MacProvider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Config {


    @Bean
    SecretKey key() {
        return MacProvider.generateKey();
    }

    public static void main(String[] args) {
        SpringApplication.run(Config.class, args);
    }
}