package com.codementor.authentication;

import java.util.Objects;
import java.util.stream.Collectors;

import com.codementor.authentication.token.TokenService;
import com.codementor.response.HeaderService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.validator.routines.EmailValidator;
import org.passay.RuleResult;
import org.passay.RuleResultDetail;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class Users {


    private final TokenService tokenService;
    private final UserRepository repository;
    private final PasswordValidationService passwordValidationService;
    private final HeaderService headerService;

    @RequestMapping(method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<TokenDto> signUp(RequestEntity<UserDto> userRequest) {
        long start = System.nanoTime();
        UserDto body = userRequest.getBody();
        String password = body.getPassword();
        String email = body.getEmail();
        if (!EmailValidator.getInstance().isValid(email)) {
            return buildInvalidField("Invalid Email", userRequest, start);
        }
        if (Objects.isNull(body.getUsername())) {
            body.setUsername(email);
        }
        RuleResult ruleResult = passwordValidationService.validatePassword(password);
        if (!ruleResult.isValid()) {
            return buildPasswordInvalid(ruleResult, userRequest, start);
        }
        if (repository.exists(email)) {
            return buildAlreadyTakenResponse(userRequest, start);
        } else {
            TokenDto token = tokenService.buildToken(body);
            saveUser(body, token);

            return headerService.maxAgeZeroCache(userRequest, start, ResponseEntity.ok()).body(token);
        }


    }

    private ResponseEntity<TokenDto> buildInvalidField(String validationMessage, RequestEntity<UserDto> userRequest, long start) {
        TokenDto token;
        ResponseEntity.BodyBuilder response = ResponseEntity.status(HttpStatus.BAD_REQUEST);
        headerService.maxAgeZeroCache(userRequest, start, response);
        token = TokenDto.builder().validation(validationMessage).build();
        return response.body(token);
    }


    private void saveUser(UserDto body, TokenDto token) {
        body.setRefreshToken(token.getRefreshToken());
        repository.saveAndFlush(body);
    }


    private ResponseEntity<TokenDto> buildAlreadyTakenResponse(RequestEntity<UserDto> userRequest, long start) {

        return headerService.maxAgeZeroCache(userRequest, start, ResponseEntity.status(HttpStatus.CONFLICT))
                            .body(TokenDto.builder()
                                          .validation("Email Already Taken")
                                          .build());
    }

    private ResponseEntity<TokenDto> buildPasswordInvalid(RuleResult validationResult, RequestEntity<UserDto> userRequest, long start) {
        return headerService.maxAgeZeroCache(userRequest, start, ResponseEntity.status(HttpStatus.BAD_REQUEST)).body(
            TokenDto.builder()
                    .validation(validationResult.getDetails()
                                                .stream()
                                                .map(RuleResultDetail::getErrorCode)
                                                .collect(Collectors.toList())
                                                .toString())
                    .build());
    }


}
