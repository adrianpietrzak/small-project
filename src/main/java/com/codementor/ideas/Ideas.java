package com.codementor.ideas;

import java.util.List;
import java.util.Optional;
import javax.validation.constraints.NotNull;

import com.codementor.authentication.UserRepository;
import com.codementor.authentication.token.TokenService;
import com.codementor.response.HeaderService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/ideas")
public class Ideas {


    private final IdeaRepository ideaRepository;
    private final UserRepository userRepository;
    private final TokenService tokenService;
    private final HeaderService headerService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<IdeaDto>> getIdeas(RequestEntity<Void> ideasRequest, @RequestParam(value = "page", required = false) Integer page) {
        long start = System.nanoTime();
        int evalPage = page != null ? page : 1;
        if (evalPage <= 0) {
            return headerService.maxAgeZeroCache(ideasRequest, start, ResponseEntity.badRequest()).build();
        }
        Optional<String> token = headerService.extractToken(ideasRequest);
        if (!token.isPresent() || !tokenService.isTokenValid(token)) {
            return headerService.maxAgeZeroCache(ideasRequest, start, ResponseEntity.status(HttpStatus.UNAUTHORIZED)).build();
        }
        IdeaDto example = IdeaDto.builder().user(userRepository.findOne(tokenService.extractEmail(token.get()))).build();
        Page<IdeaDto> all = ideaRepository.findAll(Example.of(example), new PageRequest(evalPage - 1, 10));

        return headerService.maxAgeZeroCache(ideasRequest, start, ResponseEntity.ok()).body(all.getContent());

    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<IdeaDto> postIdea(RequestEntity<IdeaDto> ideasRequest) {
        long start = System.nanoTime();
        Optional<String> token = headerService.extractToken(ideasRequest);
        IdeaDto body = ideasRequest.getBody();
        try {
            validateIdea(body);
        } catch (IllegalArgumentException e) {
            return headerService.maxAgeZeroCache(ideasRequest, start, ResponseEntity.status(HttpStatus.BAD_REQUEST)).body(
                IdeaDto.builder().validation(e.getMessage()).build());
        }
        if (!token.isPresent() || !tokenService.isTokenValid(token)) {
            return headerService.maxAgeZeroCache(ideasRequest, start, ResponseEntity.status(HttpStatus.UNAUTHORIZED)).body(
                IdeaDto.builder().validation("Try to log in again").build());
        }

        body.setUser(userRepository.findOne(tokenService.extractEmail(token.get())));
        body.setAverangeScore(calculateAverageScore(body));
        body.setCreatedAt(System.currentTimeMillis());

        IdeaDto out = ideaRepository.saveAndFlush(body);
        out.setUser(null);
        return headerService.maxAgeZeroCache(ideasRequest, start, ResponseEntity.ok()).body(out);
    }

    private void validateIdea(IdeaDto body) {
        if (StringUtils.isEmpty(body.getContent())) {
            throw new IllegalArgumentException("Cannot create empty idea");
        }
        if (body.getContent().length() > 255) {
            throw new IllegalArgumentException("Idea is too long, try making it shorter");
        }
        validateNumbersIdea(body.getConfidence(),"confidence");
        validateNumbersIdea(body.getImpact(),"impact");
        validateNumbersIdea(body.getEase(),"ease");
    }

    private void validateNumbersIdea(Integer value,String fieldName) {
        if (value == null) {
            throw new IllegalArgumentException(fieldName+" has to be set");
        }
        if (value < 1) {
            throw new IllegalArgumentException(fieldName+" has to bigger than 0");
        }
        if (value > 10) {
            throw new IllegalArgumentException(fieldName+"  has to less or equal to  10");
        }

    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<IdeaDto> updateIdea(RequestEntity<IdeaDto> ideasRequest, @PathVariable("id") String ideaId) {
        long start = System.nanoTime();

        Optional<String> token = headerService.extractToken(ideasRequest);
        IdeaDto body = ideasRequest.getBody();
        try {
            validateIdea(body);
        } catch (IllegalArgumentException e) {
            return headerService.maxAgeZeroCache(ideasRequest, start, ResponseEntity.status(HttpStatus.BAD_REQUEST)).body(
                IdeaDto.builder().validation(e.getMessage()).build());
        }
        if (!token.isPresent() || !tokenService.isTokenValid(token)) {
            return headerService.maxAgeZeroCache(ideasRequest, start, ResponseEntity.status(HttpStatus.UNAUTHORIZED)).body(
                IdeaDto.builder().validation("Try to log in again").build());
        }
        IdeaDto one = ideaRepository.findOne(ideaId);
        if (one == null) {
            return headerService.maxAgeZeroCache(ideasRequest, start, ResponseEntity.status(HttpStatus.NOT_FOUND)).body(
                IdeaDto.builder().validation("Idea not exists").build());
        }
        updateIdeaFromAnother(body, one);
        one = ideaRepository.saveAndFlush(one);
        one.setUser(null);
        return headerService.maxAgeZeroCache(ideasRequest, start, ResponseEntity.ok()).body(one);

    }

    private void updateIdeaFromAnother(@NotNull IdeaDto body, @NonNull IdeaDto one) {
        one.setEase(body.getEase());
        one.setConfidence(body.getConfidence());
        one.setImpact(body.getImpact());
        one.setContent(body.getContent());
        one.setAverangeScore(calculateAverageScore(body));
    }

    private double calculateAverageScore(IdeaDto body) {
        return (body.getConfidence() + body.getEase() + body.getImpact()) / 3.0;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Void> deleteIdea(RequestEntity<Void> ideasRequest, @PathVariable("id") String ideaId) {
        long start = System.nanoTime();
        Optional<String> token = headerService.extractToken(ideasRequest);
        if (!token.isPresent() || !tokenService.isTokenValid(token)) {
            return headerService.noCache(ideasRequest, start, ResponseEntity.status(HttpStatus.UNAUTHORIZED)).build();
        }
        IdeaDto one = ideaRepository.findOne(ideaId);
        if (one != null && !one.getUser().getEmail().equals(tokenService.extractEmail(token.get()))) {
            return headerService.noCache(ideasRequest, start, ResponseEntity.status(HttpStatus.UNAUTHORIZED)).build();
        }
        if (one != null) {
            ideaRepository.delete(ideaId);
        }
        return headerService.noCache(ideasRequest, start, ResponseEntity.ok()).build();
    }
}
